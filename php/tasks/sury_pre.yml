---

- name: Setup deb.sury.org repository - Install apt-transport-https
  ansible.builtin.apt:
    name: apt-transport-https
    state: present
  when: ansible_distribution_major_version is version('10', '<')

- name: Copy keyring packages
  ansible.builtin.copy:
    src: "{{ item }}"
    dest: "/tmp/{{ item }}"
    mode: "0644"
    owner: root
    group: root
  loop:
    - debsuryorg-archive-keyring_2024.02.05+0~20240205.1+debian12~1.gbp343037_all.deb
    - evolix-archive-keyring_0~2024_all.deb

- name: Install Evolix keyring package

- name: Install keyring packages
  ansible.builtin.apt:
    deb: "/tmp/{{ item }}"
    state: present
  loop:
    - debsuryorg-archive-keyring_2024.02.05+0~20240205.1+debian12~1.gbp343037_all.deb
    - evolix-archive-keyring_0~2024_all.deb
    # - https://packages.sury.org/php/pool/main/d/debsuryorg-archive-keyring/debsuryorg-archive-keyring_2024.02.05%2B0~20240205.1%2Bdebian12~1.gbp343037_all.deb
    # - https://pub.evolix.org/evolix/pool/main/e/evolix-archive-keyring/evolix-archive-keyring_0~2024_all.deb

- name: Add Evolix failsafe repository for Sury (Debian <12)
  ansible.builtin.apt_repository:
    repo: "deb [signed-by=/usr/share/keyrings/pub_evolix.asc] https://pub.evolix.org/evolix {{ ansible_distribution_release }}-php{{ php_version | replace('.', '')}} main"
    filename: evolix-php
    state: present
  when: ansible_distribution_major_version is version('12', '<')

- name: Add Evolix failsafe repository for Sury (Debian >=12)
  ansible.builtin.template:
    src: evolix_sury.sources.j2
    dest: /etc/apt/sources.list.d/evolix_sury.sources
    force: true
    mode: "0644"

- name: Add Sury repository (Debian <12)
  ansible.builtin.apt_repository:
    repo: "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ {{ ansible_distribution_release }} main"
    filename: sury
    state: present
    update_cache: yes
  when: ansible_distribution_major_version is version('12', '<')

- name: Add Sury repository (Debian >=12)
  ansible.builtin.template:
    src: sury.sources.j2
    dest: /etc/apt/sources.list.d/sury.sources
    mode: "0644"
    force: true
  register: sury_sources
  when: ansible_distribution_major_version is version('12', '>=')

- name: Update APT cache
  ansible.builtin.apt:
    update_cache: yes
  when: sury_sources is changed

- name: "Override package list for Sury (Debian 9 or later)"
  ansible.builtin.set_fact:
    php_stretch_packages:
      - php{{ php_version }}-cli
      - php{{ php_version }}-gd
      - php{{ php_version }}-intl
      - php{{ php_version }}-imap
      - php{{ php_version }}-ldap
      - php{{ php_version }}-mysql
      # php-mcrypt is no longer packaged for PHP 7.2
      - php{{ php_version }}-pgsql
      - php{{ php_version }}-gettext
      - php{{ php_version }}-curl
      - php{{ php_version }}-ssh2
#      - composer
#      - libphp-phpmailer
