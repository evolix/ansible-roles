#!/usr/bin/bash
#

function _get_wrappers_names() {
    grep "alerts_wrapper" --no-filename --no-messages -R /etc/nagios/ | grep --invert-match --extended-regexp "^\s*#" | awk '{ for (i=1 ; i<=NF; i++) { if ($i ~ /^(-n|--name)$/) { print $(i+1); break } } }' | tr ',' '\n' | sort | uniq
}

function _get_checks_names() {
    grep --extended-regexp --no-filename --no-messages -R "command\[check_.*\]="  /etc/nagios/ | grep --invert-match --extended-regexp "^\s*#" | grep --invert-match 'hda1' | awk -F"[\\\[\\\]=]" '{sub("check_", "", $2); print $2}' | sort | uniq
}

function _monitoringctl_completion() {
    local cur=${COMP_WORDS[COMP_CWORD]};
    local prev=${COMP_WORDS[COMP_CWORD-1]};

    local action=""
    for w in "${COMP_WORDS[@]}"; do
        case "$w" in
            status|check|enable|disable|show|list|version|help)
                action="${w}"
                ;;
        esac
    done

    local words=""
    case "${action}" in
        check|show)
            if [ "${prev}" == "check" ] || [ "${prev}" == "show" ]; then
                words="$(_get_checks_names)"
            fi
            if [ "${action}" == "check" ]; then
                words="--bypass-nrpe ${words}"
            fi
            ;;
        status)
            if [ "${prev}" == "status" ]; then
                words="$(_get_checks_names)"
            fi
            ;;
        enable)
            if [ "${prev}" == "enable" ]; then
                words="$(_get_wrappers_names)"
            else
                words="--message ${words}"
            fi
            ;;
        disable)
            if [ "${prev}" == "disable" ]; then
                words="$(_get_wrappers_names)"
            elif [ "${prev}" == "-d" ] || [ "${prev}" == "--during" ]; then
                words="1d 1d12h 1h 1h30m 1m 1m30s 30s"
            else
                words="--during --message --non-interactive ${words}"
            fi
            ;;
        *)
            words="status check enable disable show list version help ${words}"
            ;;
    esac

    # Avoid double
    opts=()
    for i in ${words}; do
        for j in "${COMP_WORDS[@]:0:${#COMP_WORDS[@]}-1}"; do
            if [[ "$i" == "$j" ]]; then
                continue 2
            fi
        done
        opts+=("$i")
    done

    COMPREPLY=($(compgen -W "${opts[*]}" -- "${cur}"))
    return 0

}

complete -F _monitoringctl_completion monitoringctl

